/*!
 * \file    preferences.h
 * \brief   Header of preferences function
 * \author  Remi BERTHO
 * \date    07/12/14
 * \version 1.0.0
 */

 /*
 * preferences.h
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PREFERENCES_H_INCLUDED
#define PREFERENCES_H_INCLUDED

#include "pdf-presentation.h"
#include "file.h"
#include "main_window.h"

/*!
 * \def FILENAME_PREFERENCES_FILE
 * Define the filename of the preferences file
 */
#define FILENAME_PREFERENCES_FILE "pdf_presenter_preferences"

/*!
 * \def FOLDER_PREFERENCES_FILE
 * Define the folder of the preferences file
 */
#define FOLDER_PREFERENCES_FILE ".config"

gboolean writePreferencesFile(globalData *data);
gboolean readPreferencesFile(globalData *data);



#endif // PREFERENCES_H_INCLUDED

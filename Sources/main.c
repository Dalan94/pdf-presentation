/*!
 * \file    main.c
 * \brief   Main
 * \author  Remi BERTHO
 * \date    06/12/14
 * \version 1.0.0
 */

 /*
 * main.c
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

 #include "pdf-presentation.h"
 #include "main_window.h"
 #include "preferences.h"

gboolean openFileWithMainArgument(globalData *data,int argc, char *argv[]);

/*!
 * \fn int main(int argc, char *argv[])
 *  Begin csuper-gui.
 * \param[in] argc the number of argument.
 * \param[in] argv the array of argument.
 * \return EXIT_SUCCESS if everything is OK
 */
int main (int argc, char *argv[])
{
    globalData data;
    GError *error = NULL;
    gchar *glade_filename = NULL;
    GtkWidget *ptr_main_window;

    /* Set locales */
    bindtextdomain("pdf-presentation","./Locales");
    bind_textdomain_codeset("pdf-presentation","UTF-8");
    textdomain("pdf-presentation");
    setlocale(LC_ALL,"");

    gtk_init(&argc, &argv);

    /* Init the global data*/
    data.ptr_builder = gtk_builder_new();

    /* Load the glade file.*/
    glade_filename =  g_build_filename("pdf-presentation.glade", NULL);
    gtk_builder_add_from_file (data.ptr_builder, glade_filename, &error);
    g_free (glade_filename);
    if (error)
    {
      gint code = error->code;
      g_printerr("%s\n", error->message);
      g_error_free (error);
      return code;
    }

    gtk_builder_connect_signals (data.ptr_builder, &data);

    ptr_main_window = GTK_WIDGET(gtk_builder_get_object(data.ptr_builder,"main_window"));

    showHideParameters(NULL,NULL,&data);
    checkBeforeRunning(NULL,&data);
    addDefaultPdfpcPath(&data);
    readPreferencesFile(&data);
    openFileWithMainArgument(&data,argc,argv);

    gtk_widget_show_all(ptr_main_window);
    #ifdef WITH_PDFPC
    hidePdfpcPath(&data);
    #endif

    gtk_main();

    g_object_unref(data.ptr_builder);

    return EXIT_SUCCESS;
}


/*!
 * \fn bool openFileWithMainArgument(globalData *data,int argc, char *argv[])
 *  Open directly a PDF file if there is one in the main argument
 * \param[in] data the globalData
 * \param[in] argc the number of argument.
 * \param[in] argv the array of argument.
 * \return TRUE if everything is OK, FALSE if there is an error while loading the file
 */
gboolean openFileWithMainArgument(globalData *data,int argc, char *argv[])
{
    /* Open the file which is on second argument id there is one*/
    if (argc < 2)
        return FALSE;

    GFile *file = g_file_new_for_path (argv[1]);
    GFileInfo *file_info = g_file_query_info (file, "standard::*", 0,NULL, NULL);
    g_object_unref(file);
    if (file_info == NULL)
        return FALSE;

    const char *content_type = g_file_info_get_content_type(file_info);
    gchar *mime_type = g_content_type_get_mime_type(content_type);
    if (g_strcmp0(mime_type,"application/pdf") == 0)
    {
        GtkWidget *filechooserbutton = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"filechooserbutton_file"));
        if (!filechooserbutton)
            g_critical("Widget filechooserbutton_file is missing in file pdf-presentation.glade.");

        gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(filechooserbutton),argv[1]);
    }

    g_object_unref(file_info);
    return TRUE;
}

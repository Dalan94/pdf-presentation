/*!
 * \file    preferences.c
 * \brief   Preferences function
 * \author  Remi BERTHO
 * \date    07/12/14
 * \version 1.0.0
 */

 /*
 * preferences.c
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

 #include "preferences.h"

/*!
 * \fn gboolean writePreferencesFile(globalData *data)
 *  Write the preferences file
 * \param[in] data the globalData
 * \return TRUE if everything is OK, FALSE otherwise
 */
gboolean writePreferencesFile(globalData *data)
{
    FILE *file;
    gchar *filename;
    gint i;
    gchar pdfpc[SIZE_MAX_CMD];
    gchar *pdf_file;
    gchar pdf_file_sdt[SIZE_MAX_CMD]="";

    /* Open the file */
    #ifndef PORTABLE
    filename = g_build_filename(g_get_home_dir(),FOLDER_PREFERENCES_FILE,FILENAME_PREFERENCES_FILE,NULL);
    #else
    filename = g_build_filename(FILENAME_PREFERENCES_FILE,NULL);
    #endif // PORTABLE
    file = openFile(filename,"w+");
    if (file == NULL)
        return FALSE;

    GtkWidget *main_grid = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"main_grid"));
    if (!main_grid)
        g_critical("Widget main_grid is missing in file pdf-presentation.glade.");

    GtkWidget *option_grid = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"grid_option"));
    if (!option_grid)
        g_critical("Widget grid_option is missing in file pdf-presentation.glade.");

    /* pdfpc */
    g_stpcpy(pdfpc,gtk_entry_get_text(GTK_ENTRY(gtk_grid_get_child_at(GTK_GRID(main_grid),1,0))));
    fprintf(file,"%s\n",pdfpc);

    /* PDF file */
    pdf_file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(gtk_grid_get_child_at(GTK_GRID(main_grid),1,1)));
    if (pdf_file)
        g_stpcpy(pdf_file_sdt,pdf_file);
    fprintf(file,"%s\n",pdf_file_sdt);
    g_free(pdf_file);

    /* Spin button */
    for (i=1 ; i<=13; i++)
    {
        if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,i))))
            fprintf(file,"%d\n",1);
        else
            fprintf(file,"%d\n",0);
    }

    /* Duration */
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,1))));

    /* End time */
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,2)),0,0))));
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,2)),2,0))));

    /* Last minutes */
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,3))));

    /* Start time */
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,4)),0,0))));
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,4)),2,0))));

    /* Current size */
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,5))));

    /* Overview min size */
    fprintf(file,"%d\n",gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,6))));

    /* Notes */
    fprintf(file,"%d\n",gtk_combo_box_get_active(GTK_COMBO_BOX(gtk_grid_get_child_at(GTK_GRID(option_grid),2,7))));


    closeFile(file);

    return TRUE;
}

/*!
 * \fn gboolean readPreferencesFile(globalData *data)
 *  Read the preferences file
 * \param[in] data the globalData
 * \return TRUE if everything is OK, FALSE otherwise
 */
gboolean readPreferencesFile(globalData *data)
{
    FILE *file;
    gchar *filename;
    gint i;
    gchar buffer[SIZE_MAX_CMD]="";
    gint int_buf;

    /* Open the file */
    #ifndef PORTABLE
    filename = g_build_filename(g_get_home_dir(),FOLDER_PREFERENCES_FILE,FILENAME_PREFERENCES_FILE,NULL);
    #else
    filename = g_build_filename(FILENAME_PREFERENCES_FILE,NULL);
    #endif // PORTABLE
    file = openFile(filename,"r");
    if (file == NULL)
        return FALSE;

    GtkWidget *main_grid = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"main_grid"));
    if (!main_grid)
        g_critical("Widget main_grid is missing in file pdf-presentation.glade.");

    GtkWidget *option_grid = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"grid_option"));
    if (!option_grid)
        g_critical("Widget grid_option is missing in file pdf-presentation.glade.");

    /* pdfpc*/
    stringKey(buffer,SIZE_MAX_CMD,file);
    #ifndef WITH_PDFPC
    gtk_entry_set_text(GTK_ENTRY(gtk_grid_get_child_at(GTK_GRID(main_grid),1,0)),buffer);
    #endif // WITH_PDFPC

    /* pdf file */
    stringKey(buffer,SIZE_MAX_CMD,file);
    if (g_file_test(buffer,G_FILE_TEST_IS_REGULAR))
        gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(gtk_grid_get_child_at(GTK_GRID(main_grid),1,1)),buffer);

    /* Spin button */
    for (i=1 ; i<=13; i++)
    {
        intKey(&int_buf,file);
        gtk_switch_set_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,i)),int_buf);
    }

    /* Duration */
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,1)),int_buf);

    /* End time */
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,2)),0,0)),int_buf);
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,2)),2,0)),int_buf);

    /* Last minutes */
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,3)),int_buf);

    /* Start time */
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,4)),0,0)),int_buf);
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,4)),2,0)),int_buf);

    /* Current size */
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,5)),int_buf);

    /* Overview min size */
    intKey(&int_buf,file);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,6)),int_buf);

    /* Notes */
    intKey(&int_buf,file);
    gtk_combo_box_set_active(GTK_COMBO_BOX(gtk_grid_get_child_at(GTK_GRID(option_grid),2,7)),int_buf);


    closeFile(file);
    checkBeforeRunning(NULL,data);
    return TRUE;
}

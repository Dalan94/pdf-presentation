/*!
 * \file    main_window.c
 * \brief   Main window
 * \author  Remi BERTHO
 * \date    06/12/14
 * \version 1.0.0
 */

 /*
 * main_window.c
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

 #include "main_window.h"

/*!
 * \fn G_MODULE_EXPORT void displayAbout(GtkWidget *widget, gpointer data)
 *  Display the about window
 * \param[in] widget the widget which send the signal
 * \param[in] data the globalData
 */
G_MODULE_EXPORT void displayAbout(GtkWidget *widget, gpointer data)
{
    globalData *user_data = (globalData*) data;
    GtkWidget *window_about = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"aboutdialog"));
    if (!window_about)
        g_critical("Widget aboutdialog is missing in file pdf-presentation.glade.");

    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(window_about),PDF_PRESENTATION_VERSION);

    gtk_dialog_run (GTK_DIALOG (window_about));
    gtk_widget_hide (window_about);
}

/*!
 * \fn G_MODULE_EXPORT void displayHelp(GtkWidget *widget, gpointer data)
 *  Display the help window
 * \param[in] widget the widget which send the signal
 * \param[in] data the globalData
 */
G_MODULE_EXPORT void displayHelp(GtkWidget *widget, gpointer data)
{
    globalData *user_data = (globalData*) data;
    GtkWidget *help_dialog = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"dialog_help"));
    if (!help_dialog)
        g_critical("Widget dialog_help is missing in file pdf-presentation.glade.");

    gtk_dialog_run (GTK_DIALOG (help_dialog));
    gtk_widget_hide (help_dialog);
}

/*!
 * \fn G_MODULE_EXPORT void showHideParameters(GObject *gobject, GParamSpec *pspec,gpointer data)
 *  Show or hide parameters
 * \param[in] gobject the object which received the signal.
 * \param[in] pspec the GParamSpec of the property which changed
 * \param[in] data the globalData
 */
G_MODULE_EXPORT void showHideParameters(GObject *gobject, GParamSpec *pspec,gpointer data)
{
    globalData *user_data = (globalData*) data;

    GtkWidget *grid_option = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"grid_option"));
    if (!grid_option)
        g_critical("Widget grid_option is missing in file pdf-presentation.glade.");

    gint i;
    for (i=1 ; i< 8 ; i++)
        gtk_widget_set_sensitive(gtk_grid_get_child_at(GTK_GRID(grid_option),2,i),
            gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(grid_option),1,i))));

}


/*!
 * \fn G_MODULE_EXPORT void runPdfpc(GtkWidget *widget, gpointer data)
 *  Run pdfpc
 * \param[in] widget the widget which send the signal
 * \param[in] data the globalData
 */
G_MODULE_EXPORT void runPdfpc(GtkWidget *widget, gpointer data)
{
    globalData *user_data = (globalData*) data;
    gchar option[SIZE_MAX_CMD]="";

    GtkWidget *main_grid = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"main_grid"));
    if (!main_grid)
        g_critical("Widget main_grid is missing in file pdf-presentation.glade.");

    GtkWidget *option_grid = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"grid_option"));
    if (!option_grid)
        g_critical("Widget grid_option is missing in file pdf-presentation.glade.");

    /* pdfpc */
    gchar pdfpc[SIZE_MAX_CMD];
    g_stpcpy(pdfpc,gtk_entry_get_text(GTK_ENTRY(gtk_grid_get_child_at(GTK_GRID(main_grid),1,0))));

    /* Filename */
    gchar *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(gtk_grid_get_child_at(GTK_GRID(main_grid),1,1)));

    /* Switch screen */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,13))))
        g_stpcpy(option,"--switch-screens");

    /* Windowed */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,12))))
        g_strlcat(option," --windowed",SIZE_MAX_CMD);

    /* Single screen */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,11))))
        g_strlcat(option," --single-screen",SIZE_MAX_CMD);

    /* Black on end  */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,10))))
        g_strlcat(option," --black-on-end",SIZE_MAX_CMD);

    /* Disable compression  */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,9))))
        g_strlcat(option," --disable-compression",SIZE_MAX_CMD);

    /* Disable cache */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,8))))
        g_strlcat(option," --disable-cache",SIZE_MAX_CMD);

    /* Notes */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,7))))
    {
        switch (gtk_combo_box_get_active(GTK_COMBO_BOX(gtk_grid_get_child_at(GTK_GRID(option_grid),2,7))))
        {
        case 0:
            g_strlcat(option," --notes=left",SIZE_MAX_CMD);
            break;
        case 1:
            g_strlcat(option," --notes=right",SIZE_MAX_CMD);
            break;
        case 2:
            g_strlcat(option," --notes=top",SIZE_MAX_CMD);
            break;
        case 3:
            g_strlcat(option," --notes=bottom",SIZE_MAX_CMD);
            break;
        }
    }

    /* Overview min size */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,6))))
    {
        gchar overview_min_size[50]="";
        g_snprintf(overview_min_size,50," --overview-min-size=%d",
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,6))));
        g_strlcat(option,overview_min_size,SIZE_MAX_CMD);
    }

    /* Current size */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,5))))
    {
        gchar current_size[50]="";
        g_snprintf(current_size,50," --current-size=%d",
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,5))));
        g_strlcat(option,current_size,SIZE_MAX_CMD);
    }

    /* Start time */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,4))))
    {
        gchar start_time[50]="";
        g_snprintf(start_time,50," --start-time=%d:%d",
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,4)),0,0))),
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,4)),2,0))));
        g_strlcat(option,start_time,SIZE_MAX_CMD);
    }

    /* Last minutes */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,3))))
    {
        gchar last_minutes[50]="";
        g_snprintf(last_minutes,50," --last-minutes=%d",
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,3))));
        g_strlcat(option,last_minutes,SIZE_MAX_CMD);
    }

    /* End time */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,2))))
    {
        gchar end_time[50]="";
        g_snprintf(end_time,50," --end-time=%d:%d",
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,2)),0,0))),
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(gtk_grid_get_child_at(GTK_GRID(option_grid),2,2)),2,0))));
        g_strlcat(option,end_time,SIZE_MAX_CMD);
    }

    /* Duration */
    if (gtk_switch_get_active(GTK_SWITCH(gtk_grid_get_child_at(GTK_GRID(option_grid),1,1))))
    {
        gchar duration[50]="";
        g_snprintf(duration,50," --duration=%d",
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_grid_get_child_at(GTK_GRID(option_grid),2,1))));
        g_strlcat(option,duration,SIZE_MAX_CMD);
    }

    /* Execute the command */
    gchar cmd[SIZE_MAX_CMD]="";
    g_snprintf(cmd,SIZE_MAX_CMD,"%s \"%s\" %s",pdfpc,filename,option);
    if (system(cmd) != 0)
    {
        GtkWidget *error_dialog = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"messagedialog_error"));
        if (!error_dialog)
            g_critical("Widget messagedialog_error is missing in file pdf-presentation.glade.");

        gtk_dialog_run (GTK_DIALOG (error_dialog));
        gtk_widget_hide (error_dialog);
    }
    else
        writePreferencesFile(user_data);

    g_free(filename);
}


/*!
 * \fn G_MODULE_EXPORT void displayHelp(GtkWidget *widget, gpointer data)
 *  Check before running pdfpc
 * \param[in] widget the widget which send the signal
 * \param[in] data the globalData
 */
G_MODULE_EXPORT void checkBeforeRunning(GtkWidget *widget, gpointer data)
{
    globalData *user_data = (globalData*) data;
    gboolean ready = TRUE;

    GtkWidget *main_grid = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"main_grid"));
    if (!main_grid)
        g_critical("Widget main_grid is missing in file pdf-presentation.glade.");

    /* Filename */
    gchar *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(gtk_grid_get_child_at(GTK_GRID(main_grid),1,1)));
    if (filename == NULL)
    {
        ready = FALSE;
        setGtkLabelAttributes(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(main_grid),0,1)),-1,TRUE,100,0,0,FALSE,0,0,0);
    }
    else
        setGtkLabelAttributes(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(main_grid),0,1)),-1,FALSE,100,0,0,FALSE,0,0,0);


    /* pdfpc */
    gchar pdfpc[SIZE_MAX_CMD];
    g_stpcpy(pdfpc,gtk_entry_get_text(GTK_ENTRY(gtk_grid_get_child_at(GTK_GRID(main_grid),1,0))));
    if (g_strrstr(pdfpc,"pdfpc") == NULL && g_strrstr(pdfpc,"pdf-presenter-console") == NULL)
    {
        ready = FALSE;
        setGtkLabelAttributes(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(main_grid),0,0)),-1,TRUE,100,0,0,FALSE,0,0,0);
    }
    else
        setGtkLabelAttributes(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(main_grid),0,0)),-1,FALSE,100,0,0,FALSE,0,0,0);

    /* Run button */
    GtkWidget *run_button = GTK_WIDGET(gtk_builder_get_object(user_data->ptr_builder,"button_run"));
    if (!run_button)
        g_critical("Widget button_run is missing in file pdf-presentation.glade.");
    if (ready)
        gtk_widget_set_sensitive(run_button,TRUE);
    else
        gtk_widget_set_sensitive(run_button,FALSE);
}

/*!
 * \fn void addDefaultPdfpcPath(globalData *data)
 *  Add the default pdfpc path
 * \param[in] data the globalData
 */
void addDefaultPdfpcPath(globalData *data)
{
    GtkWidget *entry_pdfpc = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"entry_pdfpc"));
    if (!entry_pdfpc)
        g_critical("Widget entry_pdfpc is missing in file pdf-presentation.glade.");

    #ifdef WITH_PDFPC
    gtk_entry_set_text(GTK_ENTRY(entry_pdfpc),"./pdfpc");
    #else
    gtk_entry_set_text(GTK_ENTRY(entry_pdfpc),"pdfpc");
    #endif // WITH_PDFPC
}


/*!
 * \fn void hidePdfpcPath(globalData *data)
 *  Hide the in the main window pdfpc path
 * \param[in] data the globalData
 */
void hidePdfpcPath(globalData *data)
{
    GtkWidget *main_grid = GTK_WIDGET(gtk_builder_get_object(data->ptr_builder,"main_grid"));
    if (!main_grid)
        g_critical("Widget main_grid is missing in file pdf-presentation.glade.");

    gtk_widget_hide(GTK_WIDGET(gtk_grid_get_child_at(GTK_GRID(main_grid),0,0)));
    gtk_widget_hide(GTK_WIDGET(gtk_grid_get_child_at(GTK_GRID(main_grid),1,0)));
}

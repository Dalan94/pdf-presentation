/*!
 * \file    file.c
 * \brief   Files function
 * \author  Remi BERTHO
 * \date    07/12/14
 * \version 1.0.0
 */

 /*
 * file.c
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

 #include "file.h"

 /*!
 * \fn FILE *openFile(char file_name[], char mode[])
 *  Open a file with his name and with a specific mode.
 * \param[in] file_name[] the filename
 * \param[in] mode[] the mode
 * \return a pointer to the open file, NULL if there was a problem
 */
FILE *openFile(gchar file_name[], gchar mode[])
{
    FILE *ptr_file;

    if ((ptr_file=g_fopen(file_name,mode)) == (FILE *) NULL )
    {
        printf(_("\nError while opening %s\n"),file_name);
        perror("");
    }
    return ptr_file;
}

/*!
 * \fn gint closeFile(FILE *ptr_file)
 *  Close the file
 * \param[in] *ptr_file the file
 * \return 0 if everything is OK, 1 otherwise
 */
gint closeFile(FILE *ptr_file)
{
    gint i;

    i=fclose(ptr_file);
    if (i)
    {
        printf(_("Error while closing the file"));
        perror("");
    }

    return i;
}

/*!
 * \fn gchar *stringKey(char *string, gint nb_char_plus_one,FILE *file)
 *  Do a keyboarding from the file of a string with nb-char_plus_one minus 1 characters
 * \param[in,out] *string a string
 * \param[in] nb_char_plus_one the number of characters that the function will read plus one
 * \param[in] *file the file
 */
gchar *stringKey(gchar *string, gint nb_char_plus_one,FILE *file)
{
    gchar *p;

    fgets(string,nb_char_plus_one,file);

    if (string)
    p = strchr(string, '\n');

    /*If we found the newline, we deleted it*/
    if (p)
        *p = 0;

    return string;
}

/*!
 * \fn gint *intKey(gint *nb,FILE *file)
 *  Do a keyboarding from the file of an int, put 0 if the keyboarding is not an int.
 * \param[in,out] *nb the number
 * \param[in] *file the file
 */
gint *intKey(gint *nb,FILE *file)
{
    gchar string[NB_CARACT_INT];

    stringKey(string,NB_CARACT_INT,file);
    *nb=0;
    sscanf(string,"%d",nb);

    return nb;
}

/*!
 * \file    pdf-presentation.h
 * \brief   Inclusion of pdf-presentation
 * \author  Remi BERTHO
 * \date    06/12/14
 * \version 1.0.0
 */

 /*
 * pdf-presentation.h
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PDF_PRESENTATION_H_INCLUDED
#define PDF_PRESENTATION_H_INCLUDED

/*!
 * \def PORTABLE
 * Define that we compile pdf-presentation portable
 */
//#define PORTABLE

/*!
 * \def ENABLE_DEPRECIATE_FUNCTIONS
 * Define that we enable using depreciate function
 */
//#define ENABLE_DEPRECIATE_FUNCTIONS


/*!
 * \def WITH_PDFPC
 * Define that we will use pdf-presentation with pdfpc
 */
//#define WITH_PDFPC

/*!
 * \def PDF_PRESENTATION_VERSION
 * Define the version of pdf-presentation
 */
#define PDF_PRESENTATION_VERSION "1.0.0"

/*!
 * \def SIZE_MAX_CMD
 * Define the maximum size of a command
 */
#define SIZE_MAX_CMD 2000

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <glib/gi18n.h>
#include <locale.h>

/*!
 * \struct globalData
 * Represent the global data which is passed into all signal
 */
typedef struct
{
    GtkBuilder *ptr_builder;                /*!< A pointer to a GTK builder. */
}globalData;



#endif // PDF_PRESENTATION_H_INCLUDED

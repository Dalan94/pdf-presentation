/*!
 * \file    main_window.h
 * \brief   Main window
 * \author  Remi BERTHO
 * \date    06/12/14
 * \version 1.0.0
 */

 /*
 * main_window.h
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef MAIN_WINDOW_H_INCLUDED
#define MAIN_WINDOW_H_INCLUDED

#include "pdf-presentation.h"
#include "utils.h"
#include "preferences.h"

G_MODULE_EXPORT void displayAbout(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void displayHelp(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void showHideParameters(GObject *gobject, GParamSpec *pspec,gpointer data);
G_MODULE_EXPORT void runPdfpc(GtkWidget *widget, gpointer data);
G_MODULE_EXPORT void checkBeforeRunning(GtkWidget *widget, gpointer data);
void addDefaultPdfpcPath(globalData *data);
void hidePdfpcPath(globalData *data);

#endif // MAIN_WINDOW_H_INCLUDED

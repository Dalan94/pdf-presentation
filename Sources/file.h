/*!
 * \file    file.h
 * \brief   Header for the files function
 * \author  Remi BERTHO
 * \date    07/12/14
 * \version 1.0.0
 */

 /*
 * file.h
 *
 * Copyright 2014 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This file is part of pdf-presentation.
 *
 * pdf-presentation is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * pdf-presentation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "pdf-presentation.h"

#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

/*!
 * \def NB_CARACT_INT
 * Define the number of characters that need an int.
 */
#define NB_CARACT_INT 12

FILE *openFile(gchar nome[], gchar mode[]);
int closeFile(FILE *ptr_file);
gchar *stringKey(gchar *string, gint nb_char_plus_one,FILE *file);
gint *intKey(gint *nb,FILE *file);


#endif // FILE_H_INCLUDED

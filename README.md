pdf-presentation
================

English
-------

### Informations

This software is a GUI (graphical user interface) of pdfpc. See the [website of pdfpc](https://pdfpc.github.io/) to have more informations about that.

If you have any remarks or question on the use of pdf-presentation or on the code, don't hesitate to report to me. Similarly if you want to contribute on the code.

This software is a personnel exercise of a student. Thank you to be indulgent.

Please note that the Github repository is a mirror of the real repository which can be found [here](https://git.framasoft.org/Dalan94/pdf-presentation).

### Documentation

The documentation is made by Doxygen.

A PDF version lay in the Documentation folder and a HTML version [here](http://www.dalan.rd-h.fr/documentation/doc_pdf-presentation).
### Installation

If you want to compile yourself, you can use the codeblocks project.
You can also use cmake. Type `cmake .` then `make`.
To compile you have to have gettext and GTK3 installed.

The update of the translation is done via the command `./update_translations` and the compilation is done via the command `./compile_translations`.

If you want to install pdf-presentation, do `./install_pdf-presentation`. To uninstall `./uninstall_pdf-presentation`.

Think to comment or decomment PORTABLE in the file pdf-presentation.h depending on the version you want.

### Additional Information

If you any remark for improvement, do not hesitate to send it to me.

Français
--------

### Informations

Ce logiciel est une interface graphique pour pdfpc. Pour avoir plus d'informations à propos de pdfpc veuillez consulter son [site internet](https://pdfpc.github.io/)

Si vous avez des remarques ou des questions sur l'utilisation du logiciel ou le code en lui-même, n'hésiter pas à me le faire remarquer. De même si vous voulez contribuer au logiciel d'une façon ou d'une autre.

Ce logiciel a été fait en guise d'exercice personnel pendant des études donc soyez indulgent au niveau des remarques sur le code, merci.

Veuillez prendre en compte que le dépot Github est un miroir du vrai dépot qui se trouve [ici](https://git.framasoft.org/Dalan94/pdf-presentation).

### Documentation

La documentation se fait à l'aide de Doxygen.
Une version PDF se trouve dans le dossier Documentation et une version HTML se trouve [ici](http://www.dalan.rd-h.fr/documentation/doc_pdf-presentation)
Un fichier explique aussi comment sont ordonnées les données dans les fichiers .csu

### Installation

Si vous voulez compiler vous même le logiciel, vous pouvez utiliser les projets codeblocks.
Vous pouvez aussi compiler en utilisant cmake. Aller à la racine du projet avec votre terminal puis taper `cmake ."` et enfin `make`.
Pour pouvoir compiler, il faut que vous ayez d'installé gettext et GTK3.

La mise à jour de la traduction se fait via la commande `./update_translations` et la compilation de la traduction par la commande `./compile_translations`

Et si vous voulez installer pdf-presentation faites `./install_pdf-presentation` Pour le désinstaller `./uninstall_pdf-presentation`.

Penser à commenter ou décommenter la constante PORTABLE du fichier pdf-presentation.h suivant la version que vous voulez.

### Informations complémentaires

Si vous voyez des choses à améliorer, n'hésitez à me le faire parvenir.
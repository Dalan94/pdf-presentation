# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-12-13 16:19+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Sources/file.c:49
#, c-format
msgid ""
"\n"
"Error while opening %s\n"
msgstr ""

#: Sources/file.c:68
#, c-format
msgid "Error while closing the file"
msgstr ""

#: pdf-presentation.glade:77
msgid "pdf-presentation"
msgstr ""

#: pdf-presentation.glade:98
msgid "Select the path to pdfpc"
msgstr ""

#: pdf-presentation.glade:123
msgid "Choose the pdf file"
msgstr ""

#: pdf-presentation.glade:178
msgid "Option"
msgstr ""

#: pdf-presentation.glade:193
msgid "Use option"
msgstr ""

#: pdf-presentation.glade:208
msgid "Option parameters"
msgstr ""

#: pdf-presentation.glade:222
msgid "Duration in minutes of the presentation used for timer display."
msgstr ""

#: pdf-presentation.glade:223
msgid "Duration"
msgstr ""

#: pdf-presentation.glade:265
msgid "End time of the presentation. (Format: HH:MM (24h))"
msgstr ""

#: pdf-presentation.glade:266
msgid "End time"
msgstr ""

#: pdf-presentation.glade:333 pdf-presentation.glade:462
msgid ":"
msgstr ""

#: pdf-presentation.glade:350
msgid ""
"Time in minutes, from which on the timer changes its color. (Default 5 "
"minutes)"
msgstr ""

#: pdf-presentation.glade:351
msgid "Last minutes"
msgstr ""

#: pdf-presentation.glade:392
msgid ""
"Start time of the presentation to be used as a countdown. (Format: HH:MM "
"(24h))"
msgstr ""

#: pdf-presentation.glade:393
msgid "Start time"
msgstr ""

#: pdf-presentation.glade:425 pdf-presentation.glade:445
msgid "0"
msgstr ""

#: pdf-presentation.glade:479
msgid ""
"Percentage of the presenter screen to be used for the current slide. "
"(Default 60)"
msgstr ""

#: pdf-presentation.glade:480
msgid "Current size"
msgstr ""

#: pdf-presentation.glade:521
msgid "Minimum width for the overview miniatures, in pixels. (Default 150)"
msgstr ""

#: pdf-presentation.glade:522
msgid "Overview min size"
msgstr ""

#: pdf-presentation.glade:563
msgid ""
"Disable caching and pre-rendering of slides to save memory at the cost of "
"speed."
msgstr ""

#: pdf-presentation.glade:564
msgid "Disable cache"
msgstr ""

#: pdf-presentation.glade:588
msgid ""
"Disable the compression of slide images to trade memory consumption for "
"speed. (Avg. factor 30)"
msgstr ""

#: pdf-presentation.glade:589
msgid "Disable compression"
msgstr ""

#: pdf-presentation.glade:613
msgid "Add an additional black slide at the end of the presentation"
msgstr ""

#: pdf-presentation.glade:614
msgid "Black on end "
msgstr ""

#: pdf-presentation.glade:638
msgid "Force to use only one screen"
msgstr ""

#: pdf-presentation.glade:639
msgid "Single screen"
msgstr ""

#: pdf-presentation.glade:663
msgid "Run in windowed mode (devel tool)"
msgstr ""

#: pdf-presentation.glade:664
msgid "Windowed"
msgstr ""

#: pdf-presentation.glade:688
msgid "Position of notes on the pdf page (either left, right, top or bottom)"
msgstr ""

#: pdf-presentation.glade:689
msgid "Notes"
msgstr ""

#: pdf-presentation.glade:700
msgid "Switch the presentation and the presenter screen."
msgstr ""

#: pdf-presentation.glade:701
msgid "Switch screens"
msgstr ""

#: pdf-presentation.glade:742
msgid "Left"
msgstr ""

#: pdf-presentation.glade:743
msgid "Right"
msgstr ""

#: pdf-presentation.glade:744
msgid "Top"
msgstr ""

#: pdf-presentation.glade:745
msgid "Bottom"
msgstr ""

#: pdf-presentation.glade:781
msgid "Options"
msgstr ""

#: pdf-presentation.glade:803
msgid "Display the about window"
msgstr ""

#: pdf-presentation.glade:823
msgid "Display the help window"
msgstr ""

#: pdf-presentation.glade:843
msgid "Run the presentation"
msgstr ""

#: pdf-presentation.glade:869
msgid "About pdf-presentation"
msgstr ""

#: pdf-presentation.glade:878
msgid ""
"Copyright © 2014 Rémi BERTHO <remi.bertho@gmail.com>\n"
"Copyright © 2012 David Vilar for pdfpc\n"
"Copyright © 2009-2011 Jakob Westhoff for pdf-presenter-console\n"
msgstr ""

#: pdf-presentation.glade:882
msgid "A GUI of pdfpc"
msgstr ""

#: pdf-presentation.glade:884
msgid "Visit the website of pdf-presentation"
msgstr ""

#: pdf-presentation.glade:887
msgid "Rémi BERTHO"
msgstr ""

#: pdf-presentation.glade:921
msgid "Help"
msgstr ""

#: pdf-presentation.glade:949
msgid "Close the help window"
msgstr ""

#: pdf-presentation.glade:1009
msgid "Next slide"
msgstr ""

#: pdf-presentation.glade:1021
msgid "space, right, next, left mouse click"
msgstr ""

#: pdf-presentation.glade:1128
msgid "Previous slide"
msgstr ""

#: pdf-presentation.glade:1139
msgid "Jump 10 slide forward"
msgstr ""

#: pdf-presentation.glade:1150
msgid "Jump 10 slide back"
msgstr ""

#: pdf-presentation.glade:1161
msgid "Jump forward outside of current overlay"
msgstr ""

#: pdf-presentation.glade:1172
msgid "Jump back outside of current overlay"
msgstr ""

#: pdf-presentation.glade:1183
msgid "Junp to the first slide"
msgstr ""

#: pdf-presentation.glade:1194
msgid "Jump to the last slide"
msgstr ""

#: pdf-presentation.glade:1205
msgid "Ask for a page to jump to"
msgstr ""

#: pdf-presentation.glade:1217
msgid "left, right mouse click"
msgstr ""

#: pdf-presentation.glade:1229
msgid "S+space, S+right, S+next, S+left mouse click"
msgstr ""

#: pdf-presentation.glade:1241
msgid "S+left, S+right mouse click"
msgstr ""

#: pdf-presentation.glade:1253
msgid "down"
msgstr ""

#: pdf-presentation.glade:1265
msgid "up"
msgstr ""

#: pdf-presentation.glade:1277
msgid "home"
msgstr ""

#: pdf-presentation.glade:1289
msgid "end"
msgstr ""

#: pdf-presentation.glade:1301
msgid "g"
msgstr ""

#: pdf-presentation.glade:1408
msgid "Show the overview mod"
msgstr ""

#: pdf-presentation.glade:1419
msgid "Exit pdfpc"
msgstr ""

#: pdf-presentation.glade:1430
msgid "Pause the timer"
msgstr ""

#: pdf-presentation.glade:1441
msgid "Start the timer"
msgstr ""

#: pdf-presentation.glade:1452
msgid "Reset the timer"
msgstr ""

#: pdf-presentation.glade:1463
msgid "Reset the presentation"
msgstr ""

#: pdf-presentation.glade:1474
msgid "Toggle blank presentation screen"
msgstr ""

#: pdf-presentation.glade:1485
msgid "Toggle freeze presentation screen"
msgstr ""

#: pdf-presentation.glade:1497
msgid "tab, middle mouse click"
msgstr ""

#: pdf-presentation.glade:1509
msgid "escape, q"
msgstr ""

#: pdf-presentation.glade:1521
msgid "s"
msgstr ""

#: pdf-presentation.glade:1533
msgid "p"
msgstr ""

#: pdf-presentation.glade:1545
msgid "r"
msgstr ""

#: pdf-presentation.glade:1557
msgid "R"
msgstr ""

#: pdf-presentation.glade:1569
msgid "b"
msgstr ""

#: pdf-presentation.glade:1581
msgid "f"
msgstr ""

#: pdf-presentation.glade:1628
msgid "Edit note for current slide"
msgstr ""

#: pdf-presentation.glade:1639
msgid "Set current slide as end slide"
msgstr ""

#: pdf-presentation.glade:1650
msgid "Mark current slide as overlay slide"
msgstr ""

#: pdf-presentation.glade:1662
msgid "n"
msgstr ""

#: pdf-presentation.glade:1674
msgid "o"
msgstr ""

#: pdf-presentation.glade:1686
msgid "e"
msgstr ""

#: pdf-presentation.glade:1711
msgid "<b><span size=\"x-large\">Frenquently used key on pdfpc:</span></b>"
msgstr ""

#: pdf-presentation.glade:1734
msgid "Error"
msgstr ""

#: pdf-presentation.glade:1743
msgid "Error while running pdfpc"
msgstr ""
